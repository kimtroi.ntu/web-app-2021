import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// LightBootstrap plugin
import LightBootstrap from './light-bootstrap-main'

Vue.prototype.$apiAdress = 'http://127.0.0.1:8000'
Vue.config.performance = true
Vue.config.productionTip = false
Vue.use(LightBootstrap)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
