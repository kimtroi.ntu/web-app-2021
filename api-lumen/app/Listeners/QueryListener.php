<?php

namespace App\Listeners;

use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\Facades\Log;

class QueryListener
{
    /**
     * Write file path
     * @var string
     */
    protected $writeFile;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->writeFile = storage_path() . DIRECTORY_SEPARATOR . "logs" . DIRECTORY_SEPARATOR . "sql-" . date ("Ymd") . ".log"; //Define the log path of the output
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\ExampleEvent  $event
     * @return void
     */
    public function handle(QueryExecuted $event)
    {
        $query = $event->sql; // Get the SQL statement
        foreach ($event->bindings as $bind) {
            $query = preg_replace('/\?/', ( is_numeric($bind) ? $bind : '\'' . $bind . '\'' ), $query, 1);
        }
        // Log::info("query: {$query} time: {$event->time} ms");
        file_put_contents($this->writeFile, "Query: {$query} time: {$event->time} ms" . PHP_EOL, FILE_APPEND);
    }
}
