<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;

class TodosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $todos = Todo::orderBy('id', 'DESC')->get()->toArray();

        return view('todos.index', ['todos' => $todos]);
    }

    public function addTodo(Request $request)
    {
        $todo = new Todo();
        $todo->name = $request->input('name');
        $todo->is_completed = false;
        $todo->save();

        return redirect()->route('todos');
    }

    public function changeStatus(Request $request) 
    {
        $todo = Todo::findOrFail($request->input('id'));
        $todo->is_completed = !$request->input('status');
        $todo->save();

        return response()->json($todo, 200);
    }

    public function deleteTodo($id)
    {
        $todo = Todo::findOrFail($id);
        $todo->delete();

        return redirect()->route('todos');
    }
}
