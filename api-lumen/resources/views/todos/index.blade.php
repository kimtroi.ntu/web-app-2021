<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Todo List</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        input:checked + label {
            text-decoration: line-through;
        }
    </style>
</head>

<body>
    <div class="container-md py-5">
        <div class="card">
            <div class="card-body">
                <form method="post" action="{{ route('add-todo') }}">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="" aria-describedby="helpName">
                        <small id="helpName" class="text-muted">Input your todo</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
        <div class="card mt-3">
            <div class="card-body">
                <ul class="list-group">
                    @foreach ( $todos as $todo )
                    <li class="d-flex list-group-item align-items-center">
                        <input id="todo-{{ $todo['id'] }}" class="mr-2" type="checkbox"{{ $todo['is_completed'] ? ' checked' : '' }} autocomplete="off" onchange="changeState(`{{ $todo['id'] }}`, `{{ $todo['is_completed'] }}`)">
                        <label for="todo-{{ $todo['id'] }}" class="m-0">{{$todo['name']}}</label>
                        <a href="{{ route('delete-todo', ['id' => $todo['id']]) }}" class="btn btn-danger justify-self-end ml-auto">x</a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <script defer>
        // Example POST method implementation:
        async function postData(url = '', data = {}) {
            // Default options are marked with *
            const response = await fetch(url, {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json'
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: 'follow', // manual, *follow, error
                referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
                body: JSON.stringify(data) // body data type must match "Content-Type" header
            });
            return response.json(); // parses JSON response into native JavaScript objects
        }

        // postData('https://example.com/answer', {
        //         answer: 42
        //     })
        //     .then(data => {
        //         console.log(data); // JSON data parsed by `data.json()` call
        //     });

        function changeState(id, status) {
            console.log(id, status);
            postData(`{{ route('change-status') }}`, {
                    id, 
                    status
                })
                .then(data => {
                    console.log(data); // JSON data parsed by `data.json()` call
                });
        }
    </script>
</body>

</html>