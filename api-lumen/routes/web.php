<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('todos', [
    // 'middleware' => 'age', 
    'uses' => 'TodosController@index',
    'as' => 'todos'
]);

$router->post('add-todo', [
    'uses' => 'TodosController@addTodo',
    'as' => 'add-todo'
]);

$router->post('change-status', [
    'uses' => 'TodosController@changeStatus',
    'as' => 'change-status'
]);

$router->get('delete-todo/{id}', [
    'uses' => 'TodosController@deleteTodo',
    'as' => 'delete-todo'
]);

$router->group(
    // ['middleware' => 'auth'], 
    ['prefix' => 'api'],
    function ($router) {
        $router->post('login', 'AuthController@login');
        $router->post('logout', 'AuthController@logout');
        $router->post('refresh', 'AuthController@refresh');
        $router->post('register', 'AuthController@register');
    }
);